---
openapi: 3.0.0
info:
  title: Core Service for Relex.Coin project
  version: 0.0.1-SNAPSHOT
servers:
- url: http://localhost:9001/api
  description: Generated server url
tags:
- name: Users API
  description: Эндпоинты для взаимодействия с пользователем
- name: ThanksHistory API
  description: Эндпоинты для взаимодействия с историей благодарностей пользователя
- name: Roles API
  description: Эндпоинты для взаимодействия с ролями пользователя
- name: Images API
  description: Эндпоинты для взаимодействия с фотографиями пользователя
paths:
  "/users":
    put:
      tags:
      - Users API
      summary: Обновить информацию о пользователе
      operationId: updateUser
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/UserDtoRequest"
        required: true
      responses:
        '200':
          description: OK
    post:
      tags:
      - Users API
      summary: Сохранить нового пользователя
      operationId: saveUser
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/UserDtoRequest"
        required: true
      responses:
        '201':
          description: Created
  "/roles/user/{id}":
    get:
      tags:
      - Roles API
      summary: Получить роли пользователя по id пользователя
      operationId: getRolesByUserId
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/RoleResponse"
    put:
      tags:
      - Roles API
      summary: Добавить роли для пользователя
      operationId: updateRoles
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/RoleRequest"
        required: true
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/RoleResponse"
    post:
      tags:
      - Roles API
      summary: Сохранить роли пользователя (есть описание внутри)
      description: Предыдущие роли при этом удаляются. Переданный пустой массив ролей
        будет означать, что пользователь лишится всех ролей
      operationId: saveRoles
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/RoleRequest"
        required: true
      responses:
        '201':
          description: Created
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/RoleResponse"
  "/images/user/{id}":
    get:
      tags:
      - Images API
      summary: Получить все фотографии пользователя по id пользователя
      operationId: getImagesByUserId
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/ImageResponse"
    put:
      tags:
      - Images API
      summary: Добавить фотографии к профилю пользователя. В ответе будут содержаться
        только id переданных для добавления фотографий
      operationId: updateImages
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: images
        in: query
        required: true
        schema:
          type: array
          items:
            type: string
            format: binary
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/ImageResponse"
    post:
      tags:
      - Images API
      summary: Сохранить фотографии пользователя по id пользователя (есть описание
        внутри)
      description: Первая переданная фотография становится главной фотографией пользователя.
        Все предыдущие фотографии, которые принадлежали пользователю, удаляются
      operationId: saveImages
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: images
        in: query
        required: true
        schema:
          type: array
          items:
            type: string
            format: binary
      responses:
        '201':
          description: Created
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/ImageResponse"
    delete:
      tags:
      - Images API
      summary: Удалить фотографии пользователя по переданным id
      operationId: deleteImages
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/ImageDeleteRequest"
        required: true
      responses:
        '200':
          description: OK
  "/images/main/user/{id}":
    post:
      tags:
      - Images API
      summary: Сохранить главное фото пользователя. В ответе будет содержаться только
        id главного фото
      operationId: saveMainImage
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          multipart/form-data:
            schema:
              required:
              - mainImage
              type: object
              properties:
                mainImage:
                  type: string
                  format: binary
      responses:
        '201':
          description: Created
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/ImageResponse"
  "/users/{id}":
    get:
      tags:
      - Users API
      summary: Получить информацию о пользователе по id (за исключением не главных
        фотографий и ролей)
      operationId: getUserById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/UserDtoResponse"
    delete:
      tags:
      - Users API
      summary: Уволить пользователя
      operationId: dismissUser
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        '200':
          description: OK
  "/thanks/history/user/{id}":
    get:
      tags:
      - ThanksHistory API
      summary: Получить историю благодарностей пользователя с возможностью пагинации
      operationId: getUserThankHistory
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: currentPage
        in: query
        required: false
        schema:
          type: integer
          format: int32
          default: 0
      - name: pageSize
        in: query
        required: false
        schema:
          type: integer
          format: int32
          default: 10
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/ThankHistoryResponse"
  "/images/{id}":
    get:
      tags:
      - Images API
      summary: Получить фотографию пользователя по id фотографии
      operationId: getImageById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        '200':
          description: OK
          content:
            image/png:
              schema:
                type: array
                items:
                  type: string
                  format: byte
            image/jpeg:
              schema:
                type: array
                items:
                  type: string
                  format: byte
components:
  schemas:
    UserDtoRequest:
      required:
      - address
      - birthday
      - city
      - email
      - firstName
      - lastName
      - status
      - systemId
      - username
      type: object
      properties:
        systemId:
          type: integer
          format: int64
        username:
          maxLength: 50
          minLength: 0
          type: string
        email:
          maxLength: 50
          minLength: 0
          type: string
        lastName:
          maxLength: 50
          minLength: 0
          type: string
        firstName:
          maxLength: 50
          minLength: 0
          type: string
        patronymic:
          maxLength: 50
          minLength: 0
          type: string
        birthday:
          type: string
          format: date
        city:
          maxLength: 50
          minLength: 0
          type: string
        address:
          maxLength: 100
          minLength: 0
          type: string
        status:
          type: string
          enum:
          - working
          - dismissed
        dismissalDate:
          type: string
          format: date
    RoleRequest:
      required:
      - roles
      type: object
      properties:
        roles:
          type: array
          items:
            type: string
            enum:
            - employee
            - system_administrator
            - store_administrator
            - event_administrator
            - accrual_administrator
            - budget_owner
            - auditor
            - event_organizer
    RoleDto:
      required:
      - displayName
      - name
      type: object
      properties:
        name:
          type: string
          enum:
          - employee
          - system_administrator
          - store_administrator
          - event_administrator
          - accrual_administrator
          - budget_owner
          - auditor
          - event_organizer
        displayName:
          type: string
    RoleResponse:
      required:
      - roles
      type: object
      properties:
        roles:
          type: array
          items:
            "$ref": "#/components/schemas/RoleDto"
    ImageResponse:
      type: object
      properties:
        mainImageId:
          type: integer
          format: int64
        additionalImageIds:
          type: array
          items:
            type: integer
            format: int64
    UserDtoResponse:
      required:
      - address
      - birthday
      - city
      - email
      - firstName
      - id
      - lastName
      - status
      - systemId
      - username
      type: object
      properties:
        id:
          type: integer
          format: int64
        systemId:
          type: integer
          format: int64
        username:
          maxLength: 50
          minLength: 0
          type: string
        email:
          maxLength: 50
          minLength: 0
          type: string
        lastName:
          maxLength: 50
          minLength: 0
          type: string
        firstName:
          maxLength: 50
          minLength: 0
          type: string
        patronymic:
          maxLength: 50
          minLength: 0
          type: string
        birthday:
          type: string
          format: date
        city:
          maxLength: 50
          minLength: 0
          type: string
        address:
          maxLength: 100
          minLength: 0
          type: string
        status:
          type: string
        dismissalDate:
          type: string
          format: date
        mainImageId:
          type: integer
          format: int64
    ThankChatCommentDto:
      required:
      - comment
      - createdAt
      - user
      type: object
      properties:
        user:
          "$ref": "#/components/schemas/ThankUserDto"
        comment:
          type: string
        createdAt:
          type: string
          format: date-time
    ThankHistoryDto:
      required:
      - comments
      - commentsCount
      - createdAt
      - id
      - operationType
      - thankAmount
      - user
      - userReaction
      - votesDown
      - votesUp
      type: object
      properties:
        id:
          type: integer
          format: int64
        operationType:
          type: string
          enum:
          - from
          - to
        user:
          "$ref": "#/components/schemas/ThankUserDto"
        comment:
          type: string
        userReaction:
          type: string
          enum:
          - like
          - dislike
          - none
        votesUp:
          type: integer
          format: int64
        votesDown:
          type: integer
          format: int64
        thankAmount:
          type: number
        commentsCount:
          type: integer
          format: int32
        comments:
          type: array
          items:
            "$ref": "#/components/schemas/ThankChatCommentDto"
        createdAt:
          type: string
          format: date-time
    ThankHistoryResponse:
      required:
      - currentPage
      - history
      - pageSize
      - totalElements
      type: object
      properties:
        history:
          type: array
          items:
            "$ref": "#/components/schemas/ThankHistoryDto"
        currentPage:
          type: integer
          format: int32
        pageSize:
          type: integer
          format: int32
        totalElements:
          type: integer
          format: int32
    ThankUserDto:
      required:
      - firstName
      - id
      - lastName
      type: object
      properties:
        id:
          type: integer
          format: int64
        lastName:
          type: string
        firstName:
          type: string
        patronymic:
          type: string
        mainImageId:
          type: integer
          format: int64
    ImageDeleteRequest:
      required:
      - imageIds
      type: object
      properties:
        imageIds:
          type: array
          items:
            type: integer
            format: int64
